
var fs = require('fs');
var app = require('./index');

var event = JSON.parse(fs.readFileSync('_sampleEvent.json', 'utf8').trim());
function endingFunction() {

    return "Successful Execution of Function";
}

var context = {};
context.done = function () {
    console.log("Lambda Function Complete");
}

app.handler(event, context, endingFunction);