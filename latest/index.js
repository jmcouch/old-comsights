//Dependencies required
var AWS = require('aws-sdk');
var jsForce = require('jsforce');
var mysql = require('mysql');

//Global Variables Region
var s3 = new AWS.S3();
var currentUserEmail = "";
var inboundQueue = [];
var dateOfTransaction = "";
var subjectLine = "";
var currentUserName = "";

var db_host = "sfdc-email.cqnaz19xiypc.us-east-1.rds.amazonaws.com";
var db_user = "com_sandbox";
var db_password = "CouchDev254!";
var db_port = "3306";
var db_schema = "sfdc-email-tracking-sandbox";

var accessToken = "";
var instanceUrl = "";

var conn = new jsForce.Connection();
var conn_user_refresh = new jsForce.Connection();

var bucketName = "";
var sfdc_login_url = "";
var sfdc_user = "";
var sfdc_password = "";
var sfdc_token = "";
var user_refresh = false;
var cs_email_domain = "";
var cs_ses_email = "";
var sesNotificationMessageId = "";
//Global Variables Region End	


function endingFunction() {

    return "Successful Execution of Function";
}

exports.handler = function (event, context, endingFunction) {

    console.log('Process email');
    var sesNotification = event.Records[0].ses;
    console.log("SES Notification:\n", JSON.stringify(sesNotification, null, 2));
    sesNotificationMessageId = sesNotification.mail.messageId;
    setConfigurationVariables(refreshSFDCUsers);
    endingFunction();

};

function refreshSFDCUsers(sfdc_user_refresh) {
    if (sfdc_user_refresh) {

        var connection = mysql.createConnection({
            host: db_host,
            user: db_user,
            password: db_password,
            port: db_port,
            database: db_schema,
            debug: false
        });

        connection.connect(function (err) {
            if (err) {
                console.log("Connection Error - Message Below - Connecting to DB for SFDC User Refresh");
                console.log(err.message);
                connection.end();
                endingFunction();
            }
            else {
                console.log("DB Connection Successful - For SFDC User Refresh");
                console.log("Purging All Rows from SFDC User Table");
                var sql = "DELETE FROM SFDC_Users";
                connection.query(sql, function (err, result) {
                    if (err) {
                        console.log("Deletion Error - Message Below -");
                        console.log(err.message);
                        connection.end();
                        endingFunction();
                    }
                    else {
                        console.log("Number of records deleted: " + result.affectedRows);
                    }

                });

                console.log("Retrieving all Active Users from SFDC");
                var user_records = [];
                conn_user_refresh.loginUrl = sfdc_login_url;
                conn_user_refresh.login(sfdc_user, sfdc_password + sfdc_token, function () {

                    conn_user_refresh.query("SELECT Id, Name, Email, IsActive, Corporate_Tracking__c FROM User", function (err, result) {
                        if (err) {
                            console.log("Error retrieving Users from SFDC");
                            console.log("Error Message - " + err);
                        }
                        else {
                            console.log("Retrieved " + result.records.length + " users");
                            user_records = result.records;
                            for (var i = 0; i < result.records.length; i++) {
                                var user = result.records[i];
                                var user_object = { SFDC_ID: user.Id, Name: user.Name, EmailAddress: user.Email, IsActive: user.IsActive, CorporateTracking: user.Corporate_Tracking__c };

                                connection.query('INSERT INTO SFDC_Users SET ?', user_object, (err, res) => {
                                    if (err) {
                                        console.log(err.message);
                                        console.log("Could not Insert User with ID - " + user.Id);
                                        console.log("Exiting");
                                        connection.end();
                                        endingFunction();
                                    }
                                });
                            }

                            connection.end();
                            console.log("All SFDC Users Inserted");

                            var params = {
                                Bucket: bucketName,
                                Key: sesNotificationMessageId
                            };

                            console.log("Getting S3 Object");

                            conn.loginUrl = sfdc_login_url;
                            conn.login(sfdc_user, sfdc_password + sfdc_token, function () {
                                s3.getObject(params, processS3ObjectResult);

                                //endingFunction();
                            });


                        }

                    });
                });

            }
        });
    }
    else {
        console.log("User table does not need to be refreshed");
        var params = {
            Bucket: bucketName,
            Key: sesNotification.mail.messageId
        };

        console.log("Getting S3 Object");

        conn.loginUrl = sfdc_login_url;
        conn.login(sfdc_user, sfdc_password + sfdc_token, function () {
            s3.getObject(params, processS3ObjectResult);

            endingFunction();
        });

        endingFunction();
    }

}

function setConfigurationVariables(callback) {
    console.log("Reading Configuration Variables");
    var connection = mysql.createConnection({
        host: db_host,
        user: db_user,
        password: db_password,
        port: db_port,
        database: db_schema,
        debug: true
    });
    connection.query('SELECT * FROM Config_Variables', (err, rows) => {
        if (err) {
            console.log("Database Read Error for Config Variables Information.")
            console.log(err.message);
            console.log("Exiting");
            connection.end();
            endingFunction();
        }
        else {
            bucketName_obj = rows.find(function (obj) { return obj.Property_Name === "bucketName"; });
            sfdc_login_url_obj = rows.find(function (obj) { return obj.Property_Name === "sfdc_login_url"; });
            sfdc_user_obj = rows.find(function (obj) { return obj.Property_Name === "sfdc_user"; });
            sfdc_password_obj = rows.find(function (obj) { return obj.Property_Name === "sfdc_password"; });
            sfdc_token_obj = rows.find(function (obj) { return obj.Property_Name === "sfdc_token"; });
            user_refresh_obj = rows.find(function (obj) { return obj.Property_Name === "user_refresh"; });
            cs_email_domain_obj = rows.find(function (obj) { return obj.Property_Name === "cs_email_domain"; });
            cs_ses_email_obj = rows.find(function (obj) { return obj.Property_Name === "cs_ses_email"; });

            bucketName = bucketName_obj.Property_Value;
            sfdc_login_url = sfdc_login_url_obj.Property_Value;
            sfdc_user = sfdc_user_obj.Property_Value;
            sfdc_password = sfdc_password_obj.Property_Value;
            sfdc_token = sfdc_token_obj.Property_Value;
            user_refresh = user_refresh_obj.Property_Value;
            cs_email_domain = cs_email_domain_obj.Property_Value;
            cs_ses_email = cs_ses_email_obj.Property_Value;

            console.log("Finished reading Config Variables");
            connection.end();
            callback(user_refresh);
        }
    });
}


function findUserId(user) {
    return user.id;
}

function processS3ObjectResult(err, data) {
    if (err) {
        console.log(err, err.stack);
        endingFunction();
    }
    else {

        console.log("Parse Email into Mail Object");

        if (!data.Body.toString('ascii').includes(cs_ses_email)) {
            console.log("BCC Validation Not True");
            console.log("Exiting");
            endingFunction();
        }
        else {
            console.log("BCC Found. Begin Processing.");
        }

        const simpleParser = require('mailparser').simpleParser;
        simpleParser(data.Body.toString('ascii'), (err, mail) => {
            console.log("Email Parsed Successfully");

            currentUserEmail = mail.from.value[0].address;
            currentUserName = mail.from.value[0].name;
            console.log("Checking Inbound/Outbound Flow");
            //adding check for Inbound and Outbound
            if (currentUserEmail.includes(cs_email_domain)) {
                subjectLine = "Email Sent : ";
            }
            else {
                subjectLine = "Email Received : ";
            }


            console.log("Email Sender - " + currentUserEmail);
            dateOfTransaction = mail.date;
            console.log("Email Send Date is - " + dateOfTransaction);

            if (mail.subject) {
                if (mail.subject.length > 100) {
                    subjectLine = subjectLine + mail.subject.substring(0, 99);
                }
                else {
                    subjectLine = subjectLine + mail.subject;
                }
            }
            else {
                subjectLine = subjectLine + "Subject Line was empty";
            }

            if (currentUserEmail.includes(cs_email_domain)) {
                console.log("Outbound Flow Triggered");
                outBoundFlow(mail);
            }
            else {
                var suppressionList = ["admin", "no-reply"];
                console.log("Inbound Flow Triggered");
                console.log("Checking Suppression List");

                if (suppressionList.indexOf(currentUserEmail) >= 0) {
                    console.log("Suppression Check Valid. Exiting");
                    endingFunction();
                }
                else {
                    var messageId = mail.messageId;
                    if (messageId) {
                        var connection = mysql.createConnection({
                            host: db_host,
                            user: db_user,
                            password: db_password,
                            port: db_port,
                            database: db_schema,
                            debug: false
                        });

                        connection.connect(function (err) {
                            if (err) {
                                console.log("Connection Error - Message Below");
                                console.log(err.message);
                                connection.end();
                                endingFunction();
                            }
                            else {
                                console.log("DB Connection Successful");

                                var messageId_Object = { Message_IDs: messageId, TimeStamp: dateOfTransaction };

                                connection.query('INSERT INTO MessageID_LOGs SET ?', messageId_Object, (err, res) => {
                                    if (err) {
                                        console.log(err.message);
                                        console.log("Message with ID - " + messageId + " has already been processed");
                                        console.log("Exiting");
                                        connection.end();
                                        endingFunction();
                                    }
                                    else {
                                        console.log('Message with ID ' + messageId + " has been written. Beginning Inbound Mail Flow");
                                        connection.end();
                                        inBoundFlow(mail);
                                    }


                                });
                            }
                        });

                    }
                    else {
                        console.log("Message ID could not be retrieved. Here is the Mail Object for review ");
                        console.log(mail);
                        console.log("Exiting");
                        connection.end();
                        endingFunction();
                    }

                }

            }



        });
    }
}

//callback is SearchForAccount
function processRecipientsforInboundFlow(recordId, accountId, recordType, directionFlag, callback) {
    var tasksToBeProcessed = [];
    console.log("Begin Inbound Queue Processing");
    var connection = mysql.createConnection({
        host: db_host,
        user: db_user,
        password: db_password,
        port: db_port,
        database: db_schema,
        debug: false
    });
    connection.query('SELECT * FROM SFDC_Users Where IsActive = 1 And CorporateTracking = 1', (err, rows) => {
        if (err) {
            console.log("Database Read Error for User Information.")
            console.log(err.message);
            console.log("Exiting");
            connection.end();
            endingFunction();
        }
        else {
            if (rows.length == 0) {
                console.log("User Table empty. ");
                console.log("Exiting");
                connection.end();
                endingFunction();
            }
            for (var i = 0; i < inboundQueue.length; i++) {

                var recipientEmail = inboundQueue[i].email;
                var user = rows.find(function (obj) { return obj.EmailAddress === recipientEmail; });
                console.log("Recipient is - " + recipientEmail);
                console.log("User Id is - " + user.SFDC_User_IDs);
                if (user.SFDC_User_IDs) {
                    var task = {};
                    if (recordType == "Lead") {
                        task = { Status: "Completed", Subject: subjectLine, Priority: "Normal", Type: "Email", WhoId: recordId, OwnerId: user.SFDC_User_IDs, ActivityDate: dateOfTransaction };
                        tasksToBeProcessed.push(task);
                    }
                    else {
                        task = { Status: "Completed", Subject: subjectLine, Priority: "Normal", Type: "Email", WhoId: recordId, WhatId: accountId, OwnerId: user.SFDC_User_IDs, ActivityDate: dateOfTransaction };
                        tasksToBeProcessed.push(task);
                    }
                }
            }

            console.log("Total Inbound Tasks to be created - " + tasksToBeProcessed.length);
            connection.end();
            if (tasksToBeProcessed.length > 0) {

                console.log("Running BULK Call to create tasks for " + recordType + " Record Type");
                conn.sobject("Task").create(tasksToBeProcessed, function (err, rets) {
                    if (err) {
                        console.log("Problem Creating Lead - " + err.stack + " - " + err.message);
                        console.log("Exiting");
                        endingFunction();
                    }
                    if (recordType == "Lead") {
                        for (var x = 0; x < rets.length; x++) {
                            console.log("Task Created for Lead Record with ID " + rets[x].id);
                        }
                        tasksToBeProcessed.length = 0;
                        callback(toAddressDomain, email, currentUserName, directionFlag, searchForContact);
                    }
                    else {
                        for (var x = 0; x < rets.length; x++) {
                            console.log("Task Created for Contact Record with ID " + rets[x].id);
                        }
                    }


                });
            }
        }

    });

}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function removeDuplicates(data) {
    var uniqueValues = data.filter(onlyUnique);
    return uniqueValues;
}

function inBoundFlow(mail) {
    if (inboundQueue.length > 0) {
        console.log("Need to clear out Inbound Queue. This is the current Queue");
        console.log(inboundQueue);
        console.log("Clearing");
        inboundQueue.length = 0;
        console.log("Inbound queue has been cleared out. Here's the current Queue");
        console.log(inboundQueue);

    }

    if (mail.to.value.length > 0) {
        for (var i = 0; i < mail.to.value.length; i++) {
            var emailAddress = mail.to.value[i].address;
            var fullName = mail.to.value[i].name;
            var record = { name: fullName, email: emailAddress };
            if (record.email.includes(cs_email_domain)) {
                inboundQueue.push(record);
            }
        }
    }

    if (mail.cc) {
        if (mail.cc.value.length > 0) {
            for (var i = 0; i < mail.cc.value.length; i++) {
                var emailAddress = mail.cc.value[i].address;
                var fullName = mail.cc.value[i].name;
                var record = { name: fullName, email: emailAddress };
                if (record.email.includes(cs_email_domain)) {
                    inboundQueue.push(record);
                }

            }
        }
    }

    console.log("Total Inbound Records to be processed - " + inboundQueue.length);
    console.log("This is the inbound Queue");
    console.log(inboundQueue);


    (function () {
        console.log("Processing Email Sender Record - " + currentUserEmail + " and Full Name - " + currentUserName);
        console.log("Processing Email Sender Record - " + currentUserEmail + " and Full Name - " + currentUserName);
        searchForLead(currentUserEmail, currentUserName, "inBound", leadProcessing);
    })();

}



function outBoundFlow(mail) {
    var emailsToProcess = [];

    if (mail.to.value.length > 0) {
        for (var i = 0; i < mail.to.value.length; i++) {
            var emailAddress = mail.to.value[i].address;
            var fullName = mail.to.value[i].name;
            var record = { name: fullName, email: emailAddress };
            if (!record.email.includes("@crosslakefibre.ca")) {
                emailsToProcess.push(record);
            }
        }
    }

    if (mail.cc) {
        if (mail.cc.value.length > 0) {
            for (var i = 0; i < mail.cc.value.length; i++) {
                var emailAddress = mail.cc.value[i].address;
                var fullName = mail.cc.value[i].name;
                var record = { name: fullName, email: emailAddress };
                if (!record.email.includes("@crosslakefibre.ca")) {
                    emailsToProcess.push(record);
                }

            }
        }
    }

    console.log("Total Outbound Records to be processed - " + emailsToProcess.length);

    for (var i = 0; i < emailsToProcess.length; i++) {

        (function () {
            var emailAddress = emailsToProcess[i].email;
            var fullName = emailsToProcess[i].name;
            console.log("Processing Email Recipient Record - " + emailAddress + " and Full Name - " + fullName);
            searchForLead(emailAddress, fullName, "outBound", leadProcessing);
        })();
    }
}


//callback is leadProcessing
function searchForLead(email, fullName, directionFlag, callback) {
    (function () {
        //console.log(email);
        console.log("Searching for Lead Record with Email - " + email);
        var leadRecords = conn.query("SELECT Id, Name, ConvertedContactId, IsConverted, Email FROM Lead Where Email = '" + email + "'", function (err, result) {
            if (err) {
                console.log("Error executing Lead Search " + err.message);
                console.log("Exiting");
                endingFunction();
            }
            else {
                if (directionFlag == "inBound") {
                    callback(result.records, email, fullName, directionFlag, searchForAccount);
                }
                else {
                    callback(result.records, email, fullName, directionFlag, searchForAccount);
                }

            }
        });

    })();

}

//callback is SearchForAccount
function leadProcessing(leadRecords, email, fullName, directionFlag, callback) {

    (function () {
        var toAddressDomain = email.replace(/.*@/, "");
        var connection = mysql.createConnection({
            host: db_host,
            user: db_user,
            password: db_password,
            port: db_port,
            database: db_schema,
            debug: false
        });

        connection.query('SELECT * FROM SFDC_Users Where IsActive = 1 And CorporateTracking = 1', (err, rows) => {
            if (err) {
                console.log("Database Read Error for User Information.")
                console.log(err.message);
                console.log("Exiting");
                connection.end();
                endingFunction();
            }
            else {
                if (rows.length == 0) {
                    console.log("User Table is empty.");
                    console.log("Exiting");
                    connection.end();
                    endingFunction();
                }
                if (leadRecords.length > 0) {
                    var currentLead = leadRecords[0];
                    var user = rows.find(function (obj) { return obj.EmailAddress === currentUserEmail; });
                    connection.end();
                    console.log("Found Lead Record with Email - " + email + " and ID - " + currentLead.Id);
                    if (!currentLead.IsConverted) {
                        console.log("Lead not Converted and is Valid");
                        if (directionFlag == "inBound") {
                            processRecipientsforInboundFlow(currentLead.Id, "", "Lead", directionFlag, searchForAccount);
                        }
                        else {
                            console.log("Creating Task");
                            var task = { Status: "Completed", Subject: subjectLine, Priority: "Normal", Type: "Email", WhoId: currentLead.Id, OwnerId: user.SFDC_User_IDs, ActivityDate: dateOfTransaction };
                            console.log("Lead Task Added For Processing");
                            conn.sobject("Task").create(task, function (err, ret) {
                                if (err) {
                                    console.log("Problem Creating Lead. ");
                                    console.log(err.message);
                                    console.log(err.stack);
                                    return;
                                }
                                else {
                                    console.log("Created Task with ID - " + ret.id);
                                    console.log("Begin Account Search with Domain Field = " + toAddressDomain);
                                    callback(toAddressDomain, email, fullName, directionFlag, searchForContact);
                                }

                            });
                        }

                    }
                    else {
                        //connection.end();
                        console.log("Lead with Email - " + email + " has been converted");
                        console.log("Begin Account Search with Domain Field = " + toAddressDomain);
                        callback(toAddressDomain, email, fullName, directionFlag, searchForContact);

                    }
                }
                else {
                    connection.end();
                    console.log("Unable to locate Lead with Email - " + email);
                    console.log("Begin Account Search with Domain Field = " + toAddressDomain);
                    callback(toAddressDomain, email, fullName, directionFlag, searchForContact);
                }
            }
        });

    })();


}

//callback is SearchForContact
function searchForAccount(domainField, email, fullName, directionFlag, callback) {
    console.log("Search for Account has been initiated");
    conn.query("SELECT Id, Name, Corporate_Email_Tracking__c FROM Account Where Email_Domain__c = '" + domainField + "'", function (err, result) {
        if (err) {
            console.log("Error executing Account Search Query");
            console.log("Exiting");
            endingFunction();
        }
        else {
            if (result.records.length > 0) {
                console.log("Account has been Located");
                console.log("Checking Account Disposition");
                var targetAccountId = result.records[0].Id;
                var targetAccountTrackingFlag = result.records[0].Corporate_Email_Tracking__c;

                if (!targetAccountTrackingFlag) {
                    console.log("Account Disposition did not match. Processing Ends");
                    console.log("Exiting");
                    endingFunction();
                }
                else {
                    console.log("Account Disposition is Valid");
                    callback(email, targetAccountId, directionFlag, fullName);

                }
            }
            else {
                console.log("Could not retrieve a valid Account based on Domain Name Field");
                console.log("Exiting");
                endingFunction();
            }
        }
    });
}

function searchForContact(email, accountId, directionFlag, fullName) {
    console.log("Initiated Search for Contact Underneath Valid Account");
    conn.query("SELECT Id FROM Contact Where Email = '" + email + "' AND AccountId = '" + accountId + "'", function (err, result) {
        if (err) {
            console.log("Error during Contact Query");
            console.log("Exiting");
            endingFunction();
        }
        else {
            if (result.records.length > 0) {
                if (directionFlag == "inBound") {
                    processRecipientsforInboundFlow(result.records[0].Id, accountId, "Contact", directionFlag, null);
                }
                else {
                    console.log("Contact with Email - " + email + "has been located underneath Account with ID - " + accountId)
                    var currentContact = result.records[0];
                    var task = { Status: "Completed", Subject: subjectLine, Priority: "Normal", Type: "Email", WhoId: currentContact.Id, WhatId: accountId, OwnerId: "", ActivityDate: dateOfTransaction };
                    console.log("Contact Task Added For Processing where Email is - " + email);
                    prepareForTask(task);
                }


            }
            else {
                console.log("Unable to find Associated Contacts");
                if (!fullName) {
                    fullName = email;
                    console.log("No Name detected. Using Email as Name");
                }
                console.log("Preparing to Create new Contact with Email - " + email + "and Name is " + fullName);
                createContactinSFDC(email, fullName, accountId, directionFlag);
            }
        }
    });
}

function createContactinSFDC(email, fullName, accountId, directionFlag) {
    console.log("Contact to be created with Full Name - " + fullName + " and Email - " + email + " and Account ID" + accountId);
    createNameField(fullName, function (firstName, lastName) {
        console.log("Creating contact with First Name - " + firstName + " and Last Name " + lastName);
        var contact = { OwnerId: "", AccountId: accountId, FirstName: firstName, LastName: lastName, Email: email };
        prepareForContact(contact, directionFlag);
    });
}

function createNameField(fullName, callback) {
    console.log("Parsing Full Name - " + fullName);
    if (fullName.includes('@')) {
        var firstName = "";
        console.log("First Name is " + firstName + "( Blank First Name - Using email as Name)");
        var lastName = fullName.split('@')[0];
        console.log("Last Name is " + lastName);
        callback(firstName, lastName);
    }
    else {
        if (fullName.includes('.')) {
            console.log("Name is . format");
            console.log("Custom Parsing");
            var firstName = "";
            var lastName = "";
            var name_array = fullName.split('.');
            firstName = name_array[0];
            lastName = name_array[name_array.length - 1];
            console.log("Name is " + firstName + " " + lastName);
            callback(firstName, lastName);
        }
        else {
            var parser = require('another-name-parser');
            var name = parser(fullName);
            var firstName = name.first;
            console.log("First Name is " + firstName);
            var lastName = name.last;
            console.log("Last Name is " + lastName);
            callback(firstName, lastName);
        }
    }

}

function prepareForTask(task) {
    conn.query("Select Id from User where Email = '" + currentUserEmail + "'", function (err, result) {
        task.OwnerId = result.records[0].Id;
        conn.sobject("Task").create(task, handleRecordCreation);
    });
}

function prepareForContact(contact, directionFlag) {
    var UserEmail = "";
    if (directionFlag == "inBound") {
        if (inboundQueue.length > 0) {
            UserEmail = inboundQueue[0].email;
        }
        else {
            console.log("Problem with reading Inbound Queue");
            console.log("Exiting");
            endingFunction();
        }

    }
    else {
        UserEmail = currentUserEmail;
    }

    var connection = mysql.createConnection({
        host: db_host,
        user: db_user,
        password: db_password,
        port: db_port,
        database: db_schema,
        debug: false
    });
    connection.query("SELECT * FROM SFDC_Users Where EmailAddress = '" + UserEmail + "'", (err, rows) => {
        if (err) {
            console.log("Database Read Error for User Information.")
            console.log(err.message);
            console.log("Exiting");
            connection.end();
            endingFunction();
        }
        else {
            if (rows.length == 0) {
                console.log("User Table empty. ");
                console.log("Exiting");
                connection.end();
                endingFunction();
            }
            var user = rows.find(function (obj) { return obj.EmailAddress === UserEmail; });
            contact.OwnerId = user.SFDC_ID;
            conn.sobject("Contact").create(contact, function (err, ret) {
                if (err) {
                    console.log("Problem Creating Record");
                    console.log("Exiting");
                    endingFunction();
                }
                else {
                    console.log("Contact Record created with ID - " + ret.id);
                    if (directionFlag == "inBound") {

                        processRecipientsforInboundFlow(ret.id, contact.AccountId, "Contact", directionFlag, null);
                    }
                    else {
                        var task = { Status: "Completed", Subject: subjectLine, Priority: "Normal", Type: "Email", WhoId: ret.id, WhatId: contact.AccountId, OwnerId: contact.OwnerId, ActivityDate: dateOfTransaction };
                        console.log("Contact Task Added For Processing where Email is - " + contact.Email);
                        prepareForTask(task);
                    }

                }
            });

            connection.end();

        }

    });

}

function handleRecordCreation(err, ret) {
    if (err) {
        console.log("Problem Creating Record ");
        console.log("Error Stack - " + err.stack);
        console.log("Error Message - " + err.message);
        console.log("Exiting");
        endingFunction();
    }
    else {
        console.log("Task Record created with ID - " + ret.id);
    }
}

function isInArray(value, array) {
    return array.indexOf(value) > -1;
}
